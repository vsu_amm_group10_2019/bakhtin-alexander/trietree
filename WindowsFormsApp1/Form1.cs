﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TrieTree;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Tree Tree = new Tree();
        public Form1()
        {
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string fileName = openFileDialog1.FileName;
            string[] lines = File.ReadAllLines(fileName, Encoding.GetEncoding(1251));
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
        }

        private void Redraw()
        {
            treeView1.Nodes.Clear();
            Tree.PrintToTreeView(treeView1);
            treeView1.ExpandAll();
        }

        private void countToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, int> result = Tree.Calculate();
                CountForm countForm = new CountForm(result);
                countForm.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("File is null");
            }
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tree result = Tree;
            FindForm findForm = new FindForm(result);
            findForm.ShowDialog();            
        }
    }
}
