﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrieTree;

namespace WindowsFormsApp1
{
    public partial class FindForm : Form
    {
        Tree TreeF;
        public FindForm(Tree tree)
        {
            InitializeComponent();
            TreeF = tree;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Write text");
            }
            else
            {
                textBox2.Clear();
                textBox2.Text = TreeF.Find(textBox1.Text);
            }
        }
    }
}
